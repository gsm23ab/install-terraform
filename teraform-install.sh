#!/bin/bash

WORKING_DIR=$PWD
outofdate=$(terraform version | grep -ic "out of date")
if [[ $outofdate -eq 0 ]]
        then
                echo "##################"
                echo "The terraform version is up to date"
                latest_version=$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq '.' | grep -i current_version | cut -d '"' -f4)
                current_version=$(terraform version | grep -i "terraform v"  | cut -d ' ' -f2  | tr -d 'v')
                echo ""
                echo "Current Version is - $latest_version"
                echo "Existing Version is - $current_version"
                echo ""
        else
                latest_version=$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq '.' | grep -i current_version | cut -d '"' -f4)
                down_url=$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq '.' | grep -i current_download_url | cut -d "\"" -f4)
                echo "############################"
                echo "Updating Terraform Version ----"
                echo ""
                echo "Latest Version Available - $latest_version"
                echo "Current Version in the Box - $(terraform version | grep -i "terraform v"  | cut -d ' ' -f2  | tr -d 'v')"
                echo ""
                echo "Installing the latest Terraform version ---- "
                binary_file=$(curl -s $down_url | grep -i linux | grep -i 64 | awk -F 'href' '{ print $2 }' | cut -d '>' -f2 | cut -d '<' -f1)
                full_url=$(echo $down_url$binary_file)
                curl -o terraform.zip $full_url -s
                if [ $? -eq 0 ] && [ -f $PWD/terraform.zip ]
                        then
                                unzip -o $PWD/terraform.zip
                                echo ""
                                echo "Installing Please wait"
                                for i in {0..10}
                                        do
                                                echo "." | tr '\n' ' '
                                                sleep 1
                                        done
                                echo ""
                                echo ""
                                sudo cp $WORKING_DIR/terraform /usr/local/bin
                                echo $(terraform version)
                                echo ""
                                echo "###########"
                                echo "Installation Completed"
                                echo "###########"
                                echo ""
                        else
                                echo "No Terraform file Downloaded. Please check"
                fi
fi
